from flask_cors import CORS

from app import config

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from app import config
from app.init_apps import make_celery

app = Flask(__name__)

if app.config["ENV"] == "production":
    app.config.from_object(config.ProdConfig())
else:
    app.config.from_object(config.DevConfig())

CORS(app, resources={r"/*": {"origins": "*"}})

celery = make_celery(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)


from app import views
