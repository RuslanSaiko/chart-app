from flask import jsonify, make_response

from app import app
from app.services import crypto_price_history
from app.helpers import CurrencyCode


@app.route('/healthcheck', methods=['GET'])
@app.route('/', methods=['GET'])
def healthcheck():
    return {"status": "OK"}


@app.route('/price/<currency>', methods=['GET'])
def get_price(currency: str):
    currency = currency.upper()
    if currency not in CurrencyCode.__members__:
        return {'error': f'We do not work with "{currency}" cryptocurrency. Available Values: ETH, GWEI'}, 400

    response = make_response(jsonify(crypto_price_history(currency)))
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    return response
