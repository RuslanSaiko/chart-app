from flask import jsonify

from app import app


@app.errorhandler(400)
def bad_request_handler(e):
    return jsonify(error=str(e)), 400


@app.errorhandler(404)
def bad_request_handler(e):
    return jsonify(error=str(e)), 404
