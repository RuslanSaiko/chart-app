FLASK_ENV=${FLASK_ENV:-production}
ENV_REQUIREMENTS="requirements/dev.txt"
if [ "$FLASK_ENV" = 'production' ]; then
  ENV_REQUIREMENTS="requirements/prod.txt"
fi


pip install --no-cache-dir -r "$ENV_REQUIREMENTS"