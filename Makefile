run-dev-containers:
	docker run -d -p 5432:5432 --name=postgres -e POSTGRES_PASSWORD=12345 -e POSTGRES_USER=user -e POSTGRES_DB=database postgres:12-alpine
	docker run -d -p 5672:5672 -p 15672:15672 --name=rabbitmq -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password rabbitmq:3-management

run-flask:
	PYTHONPATH=api PYTHONDONTWRITEBYTECODE=1 FLASK_APP=api/run.py FLASK_ENV=development FLASK_DEBUG=1 python -m flask run

run-celery-beat:
	PYTHONPATH=api PYTHONDONTWRITEBYTECODE=1 FLASK_APP=api/run.py FLASK_ENV=development FLASK_DEBUG=1 celery -A app.celery beat

run-celery:
	PYTHONPATH=api PYTHONDONTWRITEBYTECODE=1 FLASK_APP=api/run.py FLASK_ENV=development FLASK_DEBUG=1 celery -A app.celery worker --loglevel=info



clear-docker:
	docker container rm -f postgres rabbitmq
	yes | docker system prune -f --volumes

migrate:
	cd api && PYTHONPATH=. PYTHONDONTWRITEBYTECODE=1 FLASK_APP=api/run.py FLASK_ENV=development FLASK_DEBUG=1 flask db upgrade