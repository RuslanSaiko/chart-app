import datetime

from app import db


class CryptocurrencyPrice(db.Model):
    cc_code = db.Column(db.String(10), nullable=False, primary_key=True)
    datetime = db.Column(db.DateTime(timezone=True), nullable=False, primary_key=True, default=datetime.datetime.utcnow)
    price = db.Column(db.Float(), nullable=False)
