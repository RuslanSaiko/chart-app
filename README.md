# Test Chart App

### Run app
```
docker-compose up -d
```
Then open the link [localhost](http://localhost/)

### Start dev flask 
```
make run-flask
```

### Run migrate
```
make migrate
```

### Start celery 
```
make run-celery
```

### Start celery beat
```
make run-celery-beat
```


### Start rabbit and postgres for DEV env
```
make run-dev-containers
```

### Remove docker unused data 
```
make clear-docker
```

