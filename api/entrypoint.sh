#!/bin/sh

while ! PYTHONPATH=. PYTHONDONTWRITEBYTECODE=1 FLASK_APP=api/run.py flask db upgrade; do
  echo "try again"
done

uwsgi app.ini
