import os

import pytz


class Config:
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    API_KEY = os.getenv('API_KEY')
    API_ENDPOINT = 'https://api.etherscan.io/api'  # Leaving it here on purpose
    APP_TZ = pytz.timezone('Europe/Kiev')
    DEFAULT_TZ = pytz.timezone('GMT')
    CELERYBEAT_SCHEDULE = {
        'update_eth_price': {
            'task': 'update_eth_price',
            'schedule': 10.0
        },
        'update_gwei_price': {
            'task': 'update_gwei_price',
            'schedule': 10.0
        }
    }


class DevConfig(Config):
    DEBUG = True
    CELERY_BROKER_URL = 'pyamqp://user:password@127.0.0.1:5672'
    CELERY_BACKEND_URL = 'db+postgresql://user:12345@localhost/database'
    SQLALCHEMY_DATABASE_URI = 'postgresql://user:12345@localhost:5432/database'


class ProdConfig(Config):
    CELERY_BROKER_URL = os.getenv('CELERY_BROKER_URL')
    CELERY_BACKEND_URL = os.getenv('CELERY_BACKEND_URL')
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URI')
