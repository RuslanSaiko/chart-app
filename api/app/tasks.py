from app import celery, db
from app.helpers import CurrencyCode
from app.services import get_ethereum_price, get_gas_price, new_price


@celery.task(name="update_eth_price")
def update_eth_price():
    new_price(CurrencyCode.ETH, get_ethereum_price())


@celery.task(name="update_gwei_price")
def update_gwei_price():
    new_price(CurrencyCode.GWEI, get_gas_price())
