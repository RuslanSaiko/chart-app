FROM python:3.8-buster
ENV FLASK_ENV=production
ENV FLASK_DEBUG=0
ENV SERVER_NAME=app
RUN apt-get update -y &&\
    apt-get install -y libpq-dev &&\
    rm -rf /var/lib/apt/lists/*

WORKDIR /app
ADD . /app
RUN touch celery.pid celery-bit.pid &&\
    chmod u+x ./entrypoint.sh &&\
    sh setup.sh

EXPOSE 8080

ENTRYPOINT ["./entrypoint.sh"]