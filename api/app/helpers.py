import enum
from urllib.parse import urlencode


class EtherscanStatus(enum.IntEnum):
    NOTOK = 0
    OK = 1


class CurrencyCode(enum.Enum):
    ETH = 'ETH'
    GWEI = 'GWEI'


def url_build(url: str, params: dict = None) -> str:
    if params:
        url += f'?{urlencode(params)}'
    return url
