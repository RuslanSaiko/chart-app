import requests
import datetime

from flask import current_app

from app import db
from app.entities import CryptocurrencyPrice
from app.helpers import EtherscanStatus, CurrencyCode, url_build


def get_ethereum_price() -> float:
    result = _get_price('stats', 'ethprice')
    return float(result.get('ethusd'))


def get_gas_price() -> float:
    result = _get_price('gastracker', 'gasoracle')
    return float(result.get('ProposeGasPrice'))


def _get_price(module: str, action: str) -> dict:
    """
    Makes a request to get a price through the etherscan.io api by module and action

    For more details read official docs https://etherscan.io/apis

    :param module: Etherscan module name
    :param action: Etherscan action name
    :return:
    """
    params = {
        'module': module,
        'action': action,
        'apikey': current_app.config.get('API_KEY')
    }
    api_endpoint = current_app.config.get('API_ENDPOINT')

    uri = url_build(api_endpoint, params)
    response = requests.get(uri)
    response_content = response.json()
    if response.status_code != requests.codes.ok or int(response_content['status']) != EtherscanStatus.OK:
        raise InvalidResponse('api.etherscan.io returned unexpected response code')

    return response_content['result']


def crypto_price_history(currency: str) -> list:
    """
    Returns the history of the price of a cryptocurrency for the last 30 minutes

    :param currency: Cryptocurrency code. ETH or GWEI
    :return: Cryptocurrency price history
    """
    end_time = datetime.datetime.now(current_app.config.get('DEFAULT_TZ'))
    start_time = end_time - datetime.timedelta(minutes=30)
    app_tz = current_app.config.get('APP_TZ')

    price_history = CryptocurrencyPrice.query \
        .filter(CryptocurrencyPrice.cc_code == currency,
                CryptocurrencyPrice.datetime.between(start_time, end_time)) \
        .order_by(CryptocurrencyPrice.datetime.desc())

    return [{
        'time': price_entity.datetime.astimezone(app_tz).strftime('%Y-%m-%d %H:%M:%S'),
        'price': price_entity.price
    } for price_entity in price_history.all()[::-1]]


def new_price(code: CurrencyCode, price: float):
    """
    Creates an record in the cryptocurrency_price table with a new price for currency

    :param code: Currency code
    :param price: New price
    :return: None
    """
    db_session = db.session
    currency_model = CryptocurrencyPrice(cc_code=code.value, price=price)
    db_session.add(currency_model)
    db_session.commit()


class InvalidResponse(Exception):
    pass
